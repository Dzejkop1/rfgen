use structopt::StructOpt;
use clap::arg_enum;

use std::io::{self, prelude::*};
use std::fs::{OpenOptions};
use std::path::PathBuf;

arg_enum!{
    #[derive(Debug, Clone, Copy)]
    enum Unit {
        B,
        KB,
        MB,
        GB,
        Paragraphs,
        Words,
    }
}

fn write_binary_data<W: Write>(dest: &mut W, size: usize, unit: Unit) -> io::Result<()> {
    use rand::prelude::*;
    use Unit::*;

    let size: usize = match unit {
        Paragraphs | Words => unreachable!(),
        B => size,
        KB => size * 1024,
        MB => size * 1024usize.pow(2),
        GB => size * 1024usize.pow(3),
    };

    let mut rng = rand::thread_rng();

    let mut buffer = vec![0; size];

    rng.try_fill_bytes(&mut buffer)?;

    dest.write_all(&mut buffer).map(|_| ())
}

const MIN_PARAGRAPH_SIZE: usize = 20;
const MAX_PARAGRAPH_SIZE: usize = 120;

fn write_lipsum_data<W: Write>(dest: &mut W, size: usize, unit: Unit) -> io::Result<()> {
    use Unit::*;
    use lipsum;

    match unit {
        B | KB | MB | GB => unreachable!(),
        Words => {
            write!(dest, "{}", lipsum::lipsum_words(size))
        },
        Paragraphs => {
            use rand::Rng;
            let mut rng = rand::thread_rng();
            for _ in 0..size {
                let paragraph_size: usize = rng.gen_range(MIN_PARAGRAPH_SIZE, MAX_PARAGRAPH_SIZE);
                writeln!(dest, "{}", lipsum::lipsum_words(paragraph_size))?;
            }

            Ok(())
        },
    }
}

fn write_random_data<W: Write>(dest: &mut W, size: usize, unit: Unit) -> io::Result<()> {
    use Unit::*;
    match unit {
        B | KB | MB | GB => write_binary_data(dest, size, unit),
        Paragraphs | Words => write_lipsum_data(dest, size, unit)
    }
}

#[derive(StructOpt)]
struct Opt {
    size: usize,

    #[structopt(raw(possible_values = "&Unit::variants()", case_insensitive = "true"), default_value="B")]
    unit: Unit,

    #[structopt(help="Files to write into. If none are specified will write to standard output.")]
    output_files: Vec<PathBuf>
}

fn main() -> io::Result<()> {
    let args = Opt::from_args();

    if args.output_files.len() == 0 {
        let mut stdout = io::stdout();
        write_random_data(&mut stdout, args.size, args.unit)
    } else {
        for o in args.output_files {
            let mut file = OpenOptions::new()
                .create(true)
                .write(true)
                .open(&o)?;

            write_random_data(&mut file, args.size, args.unit)?;
        }

        Ok(())
    }
}
